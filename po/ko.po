# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2013
# 박정규(Jung-Kyu Park) <bagjunggyu@gmail.com>, 2016,2018
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2012-2013,2015-2016
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-09 00:31+0100\n"
"PO-Revision-Date: 2018-12-11 15:05+0000\n"
"Last-Translator: 박정규(Jung-Kyu Park) <bagjunggyu@gmail.com>\n"
"Language-Team: Korean (http://www.transifex.com/xfce/xfce-panel-plugins/language/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../panel-plugin/xfce4-mpc-plugin.c:125
#: ../panel-plugin/xfce4-mpc-plugin.c:201
#: ../panel-plugin/xfce4-mpc-plugin.c:792
msgid "Launch"
msgstr "실행"

#: ../panel-plugin/xfce4-mpc-plugin.c:259
msgid "Mpd Client Plugin"
msgstr "MPD 클라이언트 플러그인"

#: ../panel-plugin/xfce4-mpc-plugin.c:265
msgid "Properties"
msgstr "속성"

#: ../panel-plugin/xfce4-mpc-plugin.c:281
msgid "Host : "
msgstr "호스트 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:291
msgid "Port : "
msgstr "포트 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:301
msgid "Password : "
msgstr "암호 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:311
msgid "MPD Client : "
msgstr "MPD 클라이언트 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:320
msgid "Tooltip Format : "
msgstr "풍선 도움말 형식 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:329
msgid "Playlist Format : "
msgstr "재생 목록 형식 :"

#: ../panel-plugin/xfce4-mpc-plugin.c:338
msgid "Show _frame"
msgstr "틀 표시(_F)"

#: ../panel-plugin/xfce4-mpc-plugin.c:350
msgid "Hostname or IP address"
msgstr "호스트이름 또는 IP 주소"

#: ../panel-plugin/xfce4-mpc-plugin.c:351
msgid "Graphical MPD Client to launch in plugin context menu"
msgstr "플러그인 단축 메뉴에서 실행하는 그래픽 MPD 클라이언트"

#: ../panel-plugin/xfce4-mpc-plugin.c:352
msgid "Variables : %artist%, %album%, %file%, %track% and %title%"
msgstr "변수 :  %아티스트%, %앨범%, %트랙% 그리고 %제목%"

#: ../panel-plugin/xfce4-mpc-plugin.c:353
msgid ""
"Variables : %vol%, %status%, %newline%, %artist%, %album%, %file%, %track% "
"and %title%"
msgstr "변수 : %볼륨%, %상태%, %새줄%, %아티스트%, %앨범%, %트랙% 그리고 %제목%"

#: ../panel-plugin/xfce4-mpc-plugin.c:477
#: ../panel-plugin/xfce4-mpc-plugin.c:708
msgid ".... not connected ?"
msgstr ".... 연결하지 않았습니까 ?"

#: ../panel-plugin/xfce4-mpc-plugin.c:562
msgid "Mpd playlist"
msgstr "Mpd 재생 목록"

#: ../panel-plugin/xfce4-mpc-plugin.c:788
msgid "Random"
msgstr "임의"

#: ../panel-plugin/xfce4-mpc-plugin.c:790
msgid "Repeat"
msgstr "반복"

#: ../panel-plugin/xfce4-mpc-plugin.c:795
msgid "<b><i>Commands</i></b>"
msgstr "<b><i>명령</i></b>"

#: ../panel-plugin/xfce4-mpc-plugin.c:799
msgid "<b><i>Outputs</i></b>"
msgstr "<b><i>출력</i></b>"

#: ../panel-plugin/xfce4-mpc-plugin.c:819
msgid "A simple panel-plugin client for Music Player Daemon"
msgstr "음악 재생기 데몬을 위한 간단한 패널 플러그인 클라이언트입니다"

#: ../panel-plugin/xfce4-mpc-plugin.c:821
msgid "Copyright (c) 2006-2018 Landry Breuil\n"
msgstr "Copyright (c) 2006-2018 Landry Breuil\n"

#: ../panel-plugin/xfce4-mpc-plugin.desktop.in.h:1
msgid "MPD Client Plugin"
msgstr "MPD 클라이언트 플러그인"

#: ../panel-plugin/xfce4-mpc-plugin.desktop.in.h:2
msgid "A client for MPD, The Music Player Daemon"
msgstr "음악 재생기 데몬 MPD를 위한 클라이언트"
